﻿using Euromonitor.Core.Storage.Entity;

namespace Euromonitor.Domain.Model
{
    /// <summary>
    /// Represents the Users Subscriptions and their status
    /// -------------------------------------------------------------------------------------------------------------------------------
    /// EF Core 2 Required.
    /// It is necessary to include an entity in the model to represent the join table, 
    /// and then add navigation properties to either side of the many-to-many relations that point to the join entity instead
    /// </summary>
    public class UserBookSubscription : Entity
    {
        /// <summary>
        /// Gets a value indicating whether subscription is active.
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets the user identifier who is the owner of the subscription.
        /// </summary>
        public int UserId { get; private set; }

        /// <summary>
        /// Gets the book the subscription is for
        /// </summary>
        public int BookId { get; private set; }

        #region Navigation

        public User User { get; private set; }
        public Book Book { get; private set; }

        #endregion

        #region Constructors

        internal static UserBookSubscription AddBookToUser(User user, Book book)
        {
            return new UserBookSubscription(user, book);
        }

        private UserBookSubscription(User user, Book book)
        {
            User = user;
            Book = book;
            IsActive = true;
        }

        // ORM requires parameterless
        private UserBookSubscription()
        {
        }

        #endregion

        #region Operations

        public void MakeActive()
        {
            IsActive = true;
        }


        public void MakeInActive()
        {
            IsActive = false;
        }

        public void ToggleActive(bool isActive)
        {
            IsActive = isActive;
        }
        #endregion
    }
}
