﻿using Euromonitor.Core;
using Euromonitor.Core.Storage.Entity;
using Euromonitor.Domain.Model.Shared;

namespace Euromonitor.Domain.Model
{
    public class Book : Entity
    {
        public string Name { get; private set; }
        public string Text { get; private set; }
        public Money PurchasePrice { get; private set; }

        #region Constructors

        public static Book Create(string name, string text, Money purchasePrice)
        {
            return new Book(name, text, purchasePrice);
        }

        private Book(string name, string text, Money purchasePrice)
        {
            Check.NotNullOrEmpty(name, nameof(name));
            Check.NotNullOrEmpty(text, nameof(text));

            Name = name;
            Text = text;
            PurchasePrice = purchasePrice;
        }

        //ORM requires parameterless
        private Book()
        {
        }

        #endregion
    }
}
