﻿using System.Collections.Generic;
using Euromonitor.Core;
using Euromonitor.Core.Domain;

namespace Euromonitor.Domain.Model.Shared
{
    public class Credentials : ValueObject<Credentials>
    {
        #region Properties

        public string Email { get; private set; }
        public string Password { get; private set; }

        #endregion

        #region Constructors

        public Credentials(EmailAddress emailAddress, string password)
        {
            Check.NotNullOrEmpty(password, nameof(password));

            Email = emailAddress.Value;
            Password = password;
        }

        // AutoMapper required
        private Credentials() { }

        #endregion

        #region ValueObject Methods

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck() => new List<object> { Email, Password };

        #endregion

    }
}
