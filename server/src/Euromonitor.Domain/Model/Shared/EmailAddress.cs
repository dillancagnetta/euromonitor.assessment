﻿using System;
using System.Collections.Generic;
using Euromonitor.Core.Domain;
using Euromonitor.Core.Extensions;

namespace Euromonitor.Domain.Model.Shared
{
    public class EmailAddress : ValueObject<EmailAddress>
    {  
        public string Value { get; private set; }

        public EmailAddress( string email )
        {
            Validate( email.Trim() );
            Value = email.Trim();
        }

        private EmailAddress()
        {
        }

        private void Validate( string email )
        {
            if ( !email.IsValidEmail() ) throw new ArgumentException("Invalid email address format");
        }


        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck() => new List<object> {Value};
    }
}
