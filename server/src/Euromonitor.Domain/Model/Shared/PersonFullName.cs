﻿using System.Collections.Generic;
using Euromonitor.Core;
using Euromonitor.Core.Domain;

namespace Euromonitor.Domain.Model.Shared
{
    public class PersonFullName : ValueObject<PersonFullName>
    {
        #region Properties
        
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string FullName() => $"{FirstName} {LastName}";

        #endregion

        #region Constructors

        public static PersonFullName Create( string firstName, string lastName )
        {
            return new PersonFullName( firstName, lastName );
        }

        public static PersonFullName Create( PersonFullName fullName )
        {
            return new PersonFullName( fullName.FirstName, fullName.LastName );
        }

        private PersonFullName( string firstName, string lastName )
        {
            Check.NotNullOrEmpty(firstName, nameof(firstName));
            Check.NotNullOrEmpty(lastName, nameof(lastName));

            FirstName = firstName;
            LastName = lastName;
        }

        // Json.Net requires parameterless
        private PersonFullName() { }

        #endregion

        #region ValueObject Methods

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck() => new List<object> { FirstName, LastName };

        #endregion

        public override string ToString() => FullName();
    }
}
