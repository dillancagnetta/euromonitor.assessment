﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Euromonitor.Core.Domain;

namespace Euromonitor.Domain.Model.Shared
{
    public class Money : ValueObject<Money>, IComparable<Money>
    {        
        public decimal Amount { get; private set; }        
        public string Currency { get; private set; }

        public Money( decimal amount, string currency )
        {
            Validate( amount );

            Amount = amount;
            Currency = currency ?? new RegionInfo( CultureInfo.CurrentCulture.LCID ).ISOCurrencySymbol;
        }
        
        public Money( decimal amount )
            : this( amount, new RegionInfo( CultureInfo.CurrentCulture.LCID ).ISOCurrencySymbol )
        {
        }

        #region ORM Required
        private Money()
        {
        }

        #endregion

        #region Operations

        /// <summary>
        /// Adds the specified money.
        /// </summary>
        /// <param name="money">The money.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Currencies must be the same to be able to add.</exception>
        public Money Add( Money money )
        {
            if ( Currency != money.Currency ) throw new ArgumentException( "Currencies must be the same to be able to add." );

            return new Money( Amount + money.Amount, Currency );
        }

        #endregion

        #region Private Methods

        private void Validate( decimal amount )
        {
            if ( amount % 0.01m != 0 ) throw new MoreThanTwoDecimalPlacesInMoneyValueException();

            if ( amount < 0 ) throw new MoneyCannotBeANegativeValueException();
        }

        #endregion

        #region ValueObject Methods

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
            => new List<Object> { Amount, Currency };

        #endregion

        #region IComparable

        public int CompareTo( Money other )
        {
            return Amount.CompareTo( other.Amount ) + string.Compare( Currency, other.Currency, StringComparison.Ordinal );
        }

        #endregion

    }

    public class MoreThanTwoDecimalPlacesInMoneyValueException : Exception { }

    public class MoneyCannotBeANegativeValueException : Exception { }

}
