﻿using System.Collections.Generic;
using System.Linq;
using Euromonitor.Core.Extensions;
using Euromonitor.Core.Storage.Entity;
using Euromonitor.Domain.Model.Shared;

namespace Euromonitor.Domain.Model
{
    public class User : Entity
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string EmailAddress { get; private set; }
        public string Password { get; private set; }
        public ICollection<UserBookSubscription> Subscriptions { get; private set; } = new List<UserBookSubscription>();

        #region Constructors

        public static User Create( PersonFullName fullName, EmailAddress emailAddress, string password = null )
        {
            return new User( fullName.FirstName, fullName.LastName, emailAddress.Value, password );
        }

        private User( string firstName, string lastName, string emailAddress, string password )
        {
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            Password = password ?? "password";
        }

        //ORM requires parameterless
        private User()
        {
        }

        #endregion

        #region Operations

        public void AddBookSubscription( Book book )
        {
            var bookFound = Subscriptions.FirstOrDefault( x => x.BookId == book.Id );
            if ( bookFound != null )
            {
                bookFound.MakeActive();
                return;
            }

            Subscriptions.Add( UserBookSubscription.AddBookToUser( this, book ) );
        }

        public void UpdateBookSubscription( UserBookSubscription update )
        {
            var bookFound = Subscriptions.FirstOrDefault( x => x.BookId == update.BookId );

            bookFound?.ToggleActive(update.IsActive);
        }

        public void UpdateEmailAddress( EmailAddress email )
        {
            EmailAddress = email.Value;
        }

        public void UpdateFullName( PersonFullName fullName )
        {
            FirstName = fullName.FirstName;
            LastName = fullName.LastName;
        }

        public void UpdateFrom( User updated )
        {
            FirstName = updated.FirstName;
            LastName = updated.LastName;
            EmailAddress = updated.EmailAddress;
            Password = updated.Password;
        }

        public bool CredentialsVerified( Credentials credentials )
            => EmailAddress.IsPresent() &&
                  Password.IsPresent() &&
                  EmailAddress.Equals( credentials.Email ) &&
                  Password.Equals( credentials.Password );

        #endregion


    }
}
