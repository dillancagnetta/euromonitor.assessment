﻿using Euromonitor.Core.Storage;
using Euromonitor.Domain.Model;

namespace Euromonitor.Domain.Repositories
{
    public interface IBookRepository : IDatabaseRepository<Book>
    {         
    }
}
