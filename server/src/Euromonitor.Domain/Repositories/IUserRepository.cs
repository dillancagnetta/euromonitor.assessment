﻿using System.Threading.Tasks;
using Euromonitor.Core.Storage;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;

namespace Euromonitor.Domain.Repositories
{
    public interface IUserRepository : IDatabaseRepository<User>
    {
        Task<User> FindByEmailAddress(EmailAddress email);
        Task<User> GetWithBookSubscriptions(int userId);
    }
}
