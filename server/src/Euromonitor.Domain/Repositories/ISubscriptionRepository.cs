﻿using Euromonitor.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Euromonitor.Core.Storage;

namespace Euromonitor.Domain.Repositories
{
    public interface ISubscriptionRepository : IDatabaseRepository<UserBookSubscription>
    {
        Task<IEnumerable<UserBookSubscription>> GetUserSubscriptions(int userId, bool includeInactive=false);
    }
}
