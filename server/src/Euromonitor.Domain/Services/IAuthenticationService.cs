﻿using System.Threading.Tasks;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;
using Euromonitor.Domain.Results;

namespace Euromonitor.Domain.Services
{
    public interface IAuthenticationService
    {
        Task<AuthResult> Authenticate(Credentials credentials);
    }
}
