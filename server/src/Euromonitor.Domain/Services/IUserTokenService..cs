﻿using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Dto;

namespace Euromonitor.Domain.Services
{
    public interface IUserTokenService
    {
        UserToken GenerateEncodedToken( User user );
    }
}
