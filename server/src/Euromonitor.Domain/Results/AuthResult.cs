﻿using Euromonitor.Core.Results;
using Euromonitor.Domain.Model.Dto;

namespace Euromonitor.Domain.Results
{
    public class AuthResult : Result<UserToken>
    {
        public AuthResult(bool isSuccess, UserToken userToken) : base(isSuccess, userToken)
        {           
        }

        public new static AuthResult Success(UserToken userToken) => new AuthResult(true, userToken);
        public static AuthResult Failure() => new AuthResult(false, null);
    }
}
