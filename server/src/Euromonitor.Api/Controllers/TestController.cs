﻿using Microsoft.AspNetCore.Mvc;

namespace Euromonitor.Api.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }
    }
}
