﻿using System.Threading.Tasks;
using Euromonitor.Application;
using Euromonitor.Core.Storage.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Euromonitor.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public abstract class ApiController<TEntity, TInputModel> : ControllerBase
        where TEntity : IEntity
        where TInputModel : IInputModel
    {
        protected IApplicationService<TEntity> AppService { get; }

        protected ApiController(IApplicationService<TEntity> appService)
        {
            AppService = appService;
        }

        // GET api/[controller]
        /// <returns> All items in the database </returns>
        [HttpGet]
        public virtual async Task<IActionResult> Get()
        {
            var entities = await AppService.GetAllAsync();

            return Ok(entities);
        }

        // POST api/[controller]
        /// <returns> Creates a new entity and returns its id </returns>
        [HttpPost]
        public virtual async Task<IActionResult> Create([FromBody] TInputModel model)
        {
            var id = await AppService.CreateAsync(model);

            return Created(Request.Path, id);
        }
    }
}
