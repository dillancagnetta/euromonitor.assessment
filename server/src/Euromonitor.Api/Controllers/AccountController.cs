﻿using Euromonitor.Application.Account.Models;
using Euromonitor.Domain.Services.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Euromonitor.Api.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountAppService _accountAppService;

        public AccountController(IAccountAppService accountAppService)
        {
            _accountAppService = accountAppService;
        }

        // POST api/accounts/login
        /// <returns> authenticates a user and returns a token  </returns>
        [HttpPost("login")]
        public async Task<IActionResult> Authenticate([FromBody] CredentialsInputModel model)
        {
            var authResult = await _accountAppService.AuthenticateAsync(model);

            if (authResult.IsSuccess)
            {
                return Ok(authResult.Data);
            }

            return Unauthorized(); ;
        }

        // POST api/accounts/register
        /// <returns> registers a new user </returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationInputModel model)
        {
            var id = await _accountAppService.RegisterAsync(model);

            return Created("api/users", id);
        }
    }
}
