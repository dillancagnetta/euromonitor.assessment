﻿using Euromonitor.Application.Subscriptions;
using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Euromonitor.Api.Controllers
{
    public class SubscriptionsController : ApiController<UserBookSubscription, SubscriptionInputModel>
    {
        public SubscriptionsController(ISubscriptionsAppService appService) : base(appService) { }

        // GET api/Subscriptions/1
        /// <returns> returns all book subscriptions for a user </returns>
        [HttpGet("user/{userId:int}")]
        public async Task<IActionResult> Get(int userId, [FromQuery] bool includeInactive = false)
        {
            var subscriptions = await ((ISubscriptionsAppService)AppService).GetUserSubscriptions(userId, includeInactive);            

            return Ok(subscriptions);
        }

        // PUT api/Subscriptions/1
        /// <returns> updates subscription book status for a user </returns>
        [HttpPut("user/{userId:int}")]
        public virtual async Task<IActionResult> Update(int userId, [FromBody] SubscriptionInputModel model)
        {
            await ((ISubscriptionsAppService)AppService).UpdateUserSubscription(userId, model);

            return Ok();
        }
    }
}
