﻿using Euromonitor.Application.Books;
using Euromonitor.Application.Books.Models;
using Euromonitor.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Euromonitor.Api.Controllers
{
    public class BooksController : ApiController<Book, BookInputModel>
    {
        public BooksController(IBooksAppService appService) : base (appService) { }

        // GET api/books/user/1
        /// <returns> returns all books with users current subscription status </returns>
        [HttpGet("user/{userId:int}")]
        public async Task<IActionResult> Get(int userId)
        {
            var subscriptions = await ((IBooksAppService)AppService).GetBooksWithUserSubscriptionStatus(userId);

            return Ok(subscriptions);
        }
    }
}

