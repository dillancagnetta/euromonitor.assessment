﻿using AutoMapper;
using Euromonitor.Application.Account.Models;
using Euromonitor.Application.Books.Models;
using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;

namespace Euromonitor.Api.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CredentialsInputModel, Credentials>();            
            CreateMap<RegistrationInputModel, User>();            
            CreateMap<SubscriptionInputModel, UserBookSubscription>();
            CreateMap<MoneyInputModel, Money>();
            CreateMap<BookInputModel, Book>();

            CreateMap<UserBookSubscription, SubscriptionViewModel>().ReverseMap();
        }
    }
}
