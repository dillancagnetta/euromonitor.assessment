﻿using AutoMapper;
using Euromonitor.Application;
using Euromonitor.Application.Books;
using Euromonitor.Application.Subscriptions;
using Euromonitor.Core.Security;
using Euromonitor.Domain.Repositories;
using Euromonitor.Domain.Services;
using Euromonitor.Domain.Services.Application;
using Euromonitor.Infrastructure.Data;
using Euromonitor.Infrastructure.Repositories;
using Euromonitor.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Euromonitor.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddDatabase();
            services.AddAutoMapper();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddEuromonitorAuthentication(Configuration);

            // configure DI for services
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<ISubscriptionRepository, SubscriptionRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserTokenService, UserTokenService>();            
            services.AddScoped<IAccountAppService, AccountAppService>();        
            services.AddScoped<IBooksAppService, BooksAppService>();   
            services.AddScoped<ISubscriptionsAppService, SubscriptionsAppService>();   
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            SeedData.Initialize(app);

            // Global cors policy for demo purposes only
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
