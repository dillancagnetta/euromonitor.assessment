﻿using Euromonitor.Core.Security;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Dto;
using Euromonitor.Domain.Services;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Euromonitor.Infrastructure.Services
{
    public class UserTokenService : IUserTokenService
    {
        private readonly JwtIssuerOptions _jwtOptions;

        public UserTokenService(JwtIssuerOptions options)
        {
            _jwtOptions = options;
        }

        public UserToken GenerateEncodedToken(User user)
        {
            var claims = new[]
            {
                 new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                 new Claim(JwtRegisteredClaimNames.Email, user.EmailAddress),
                 new Claim(JwtRegisteredClaimNames.FamilyName, user.LastName),
                 new Claim(JwtRegisteredClaimNames.GivenName, user.FirstName)
             };

            var key = Encoding.ASCII.GetBytes(_jwtOptions.Secret);
            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            // return basic user info (without password) and token to store client side
            return new UserToken
            {
                Id = user.Id,
                EmailAddress = user.EmailAddress,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = encodedJwt
            };
        }
    }
}
