﻿using Euromonitor.Domain.Model;
using Euromonitor.Domain.Results;
using Euromonitor.Domain.Services;
using System.Threading.Tasks;
using Euromonitor.Domain.Model.Shared;
using Euromonitor.Domain.Repositories;

namespace Euromonitor.Infrastructure.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserTokenService _tokenService;

        public AuthenticationService(
            IUserRepository userRepository,
            IUserTokenService tokenService)
        {
            _userRepository = userRepository;
            _tokenService = tokenService;
        }

        public async Task<AuthResult> Authenticate(Credentials credentials)
        {
            var user = await _userRepository.FindByEmailAddress(new EmailAddress(credentials.Email));

            if (user != null && user.CredentialsVerified(credentials))
            {
                var userToken = _tokenService.GenerateEncodedToken(user);
                return AuthResult.Success(userToken);
            }
            
            return AuthResult.Failure();
        }
    }
}
