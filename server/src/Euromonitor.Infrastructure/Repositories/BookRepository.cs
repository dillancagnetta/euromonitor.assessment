﻿using Euromonitor.Domain.Model;
using Euromonitor.Domain.Repositories;
using Euromonitor.Infrastructure.Data;

namespace Euromonitor.Infrastructure.Repositories
{
    public class BookRepository : DatabaseRepository<Book>, IBookRepository
    {
        public BookRepository(DataContext dbContext) : base(dbContext) { }
    }
}
