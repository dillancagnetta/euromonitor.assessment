﻿using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;
using Euromonitor.Domain.Repositories;
using Euromonitor.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using TaskExtensions = Euromonitor.Core.Extensions.TaskExtensions;

namespace Euromonitor.Infrastructure.Repositories
{
    public class UserRepository : DatabaseRepository<User>, IUserRepository
    {
        public UserRepository(DataContext context): base(context) { }

        public async Task<User> FindByEmailAddress(EmailAddress email)
            => await TaskExtensions.ConfigureAwaitable(Queryable().FirstOrDefaultAsync( x => x.EmailAddress.Equals(email.Value)));

        public async Task<User> GetWithBookSubscriptions(int userId)
        {
            var user =
                await TaskExtensions.ConfigureAwaitable(
                    Queryable()
                        .Include(x => x.Subscriptions).ThenInclude(x => x.Book) // Get all we need
                        .FirstOrDefaultAsync(x => x.Id == userId)
                );

            return user;
        }
    }
}
