﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Repositories;
using Euromonitor.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Euromonitor.Infrastructure.Repositories
{
    public class SubscriptionRepository : DatabaseRepository<UserBookSubscription>, ISubscriptionRepository
    {
        public SubscriptionRepository( DataContext dbContext ) : base( dbContext )
        {
        }

        public async Task<IEnumerable<UserBookSubscription>> GetUserSubscriptions( int userId, bool includeInactive = false )
        {
            var subscriptions = await GetSubscriptions( userId );

            if ( includeInactive )
            {
                return subscriptions;
            }

            return subscriptions.Where( x => x.IsActive );
        }

        public override async Task Update( int userId, UserBookSubscription update )
        {
             var user = await GetUser(userId);

              user.UpdateBookSubscription(update);
        }

        #region Private Methods

        private async Task<IEnumerable<UserBookSubscription>> GetSubscriptions( int userId )
        {
            var user = await GetUser( userId );

            return user.Subscriptions;
        }

        private async Task<User> GetUser( int userId )
        {
            var user = await Context.Users
                .Include( x => x.Subscriptions ).ThenInclude( x => x.Book ) // Get all we need
                .FirstOrDefaultAsync( x => x.Id == userId );

            return user;
        } 

        #endregion
    }
}
