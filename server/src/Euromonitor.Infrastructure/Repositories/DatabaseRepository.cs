﻿using Euromonitor.Core.Storage;
using Euromonitor.Core.Storage.Entity;
using Euromonitor.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskExtensions = Euromonitor.Core.Extensions.TaskExtensions;

namespace Euromonitor.Infrastructure.Repositories
{
    public abstract class DatabaseRepository<T> : IDatabaseRepository<T> where T : class, IEntity
    {
        #region Fields

        internal DbSet<T> ObjectSet;

        #endregion

        #region Properties

        public DataContext Context { get; }

        #endregion

        #region Constructors

        protected DatabaseRepository( DataContext dbContext )
        {
            Context = dbContext;
            ObjectSet = Context.Set<T>();
        }

        #endregion

        public IQueryable<T> Queryable() => ObjectSet;


        #region Repository Methods

        public virtual async Task<IEnumerable<T>> GetAll() => await TaskExtensions.ConfigureAwaitable( Queryable().AsNoTracking().ToListAsync() );

        public virtual async Task<T> GetById( int id ) => await TaskExtensions.ConfigureAwaitable( ObjectSet.FindAsync( id ) );

        public virtual async Task<int> Add( T item )
        {
            await TaskExtensions.ConfigureAwaitable( ObjectSet.AddAsync( item ) );

            return item.Id;
        }

        public virtual async Task AddRange( IEnumerable<T> items )
        {
            await TaskExtensions.ConfigureAwaitable( ObjectSet.AddRangeAsync( items ) );
        }

        public virtual async Task Update( int id, T sourceItem )
        {
            var targetItem = await TaskExtensions.ConfigureAwaitable( ObjectSet.FindAsync( id ) );
            if ( targetItem != null )
            {
                Context.Entry( targetItem ).CurrentValues.SetValues( sourceItem );
            }
        }

        public virtual async Task Delete( int id )
        {
            var item = await TaskExtensions.ConfigureAwaitable( ObjectSet.FindAsync( id ) );
            if ( item != null )
            {
                ObjectSet.Remove( item );
            }
        }

        public virtual async Task<int> SaveChanges() => await TaskExtensions.ConfigureAwaitable( Context.SaveChangesAsync() );

        #endregion

        #region IDisposable
        public void Dispose()
        {
            Context.Dispose();
            GC.SuppressFinalize( this );
        }
        #endregion
    }
}
