﻿using System.Collections.Generic;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;
using Euromonitor.Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Euromonitor.Infrastructure.Data
{
    public static class SeedData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            var user = User.Create(
                    fullName: PersonFullName.Create("Dillan", "Cagnetta"),
                    emailAddress: new EmailAddress("dillancagnetta@yahoo.com"),
                    password: "password");

            var books = new List<Book>(3)
            {
                Book.Create(
                    name: "Great Expectations",
                    text: " Charles Dickens",
                    purchasePrice: new Money(200, "ZAR")
                ),
                Book.Create(
                    name: "Moby-Dick",
                    text: "Herman Melville",
                    purchasePrice: new Money(300, "ZAR")
                ),
                Book.Create(
                    name: "The Adventures of Huckleberry Finn",
                    text: "Mark Twain",
                    purchasePrice: new Money(100, "ZAR")
                ),
            };

            // Seed the Database
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataContext>();

                // First create data
                var userRepository = new UserRepository(context);
                userRepository.Add( user ).Wait();

                var bookRepository = new BookRepository(context);
                bookRepository.AddRange(books).Wait();

                context.SaveChangesAsync().Wait();


                // Now add the subscriptions
                user.AddBookSubscription(books[0]);
                context.SaveChangesAsync().Wait();
            }
        }
    }
}
