using Euromonitor.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Euromonitor.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<UserBookSubscription> Subscriptions { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            ConfigureBooks(builder);
            ConfigureUserBooksSubscriptions(builder);
        }

        #region Configuration

        private void ConfigureUserBooksSubscriptions(ModelBuilder builder)
        {
            var entity = builder.Entity<UserBookSubscription>();
            // Remove because we will use a composite key
            entity.Ignore(b => b.Id);
            // Which is this 
            entity.HasKey(bc => new { bc.BookId, bc.UserId });

            /*
            * OnDelete Restrict will prevent books being removed when a user is deleted
            */
            entity.HasOne(bc => bc.User)
                      .WithMany(b => b.Subscriptions)
                      .HasForeignKey(bc => bc.UserId)
                      .OnDelete(DeleteBehavior.Restrict);
        }

        private void ConfigureBooks(ModelBuilder builder)
        {
            var entity = builder.Entity<Book>();

            entity.OwnsOne(x => x.PurchasePrice);
        } 

        #endregion
    }
}