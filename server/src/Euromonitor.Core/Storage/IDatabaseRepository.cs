﻿using Euromonitor.Core.Storage.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Euromonitor.Core.Storage
{
    public interface IDatabaseRepository<TEntity> : IDisposable where TEntity : IEntity
    {
        IQueryable<TEntity> Queryable();
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task<int> Add(TEntity entity);
        Task AddRange(IEnumerable<TEntity> entities);
        Task Update(int id,  TEntity entity);
        Task Delete(int id);
        Task<int> SaveChanges();
    }
}
