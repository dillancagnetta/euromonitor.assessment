﻿namespace Euromonitor.Core.Storage.Entity
{
    public interface IEntity
    {
         int Id { get; }
    }
}
