﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Euromonitor.Core.Security
{
    public static class ServiceCollectionExtensions
    {
        public static void AddEuromonitorAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            // Get options from app settings
            var jwtOptions = new JwtIssuerOptions();
            configuration.GetSection(nameof(JwtIssuerOptions)).Bind(jwtOptions);
            // Add to IoC container
            services.AddSingleton<JwtIssuerOptions>(jwtOptions);

            // Jwt wire up
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                   .AddJwtBearer(options =>
                  {
                      options.TokenValidationParameters =
                           new TokenValidationParameters
                           {
                               ValidateIssuer = true,
                               ValidateAudience = true,
                               ValidateLifetime = true,
                               ValidateIssuerSigningKey = true,

                               ValidIssuer = jwtOptions.Issuer,
                               ValidAudience = jwtOptions.Audience,
                               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Secret))
                           };
                  });            
        }
    }
}
