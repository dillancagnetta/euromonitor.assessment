using System;

namespace Euromonitor.Core.Exceptions
{
    public class EuromonitorException : Exception
    {
        public string Code { get; }

        public EuromonitorException()
        {
        }

        public EuromonitorException(string code)
        {
            Code = code;
        }

        public EuromonitorException(string message, params object[] args)
            : this(string.Empty, message, args)
        {
        }

        public EuromonitorException(string code, string message, params object[] args)
            : this(null, code, message, args)
        {
        }

        public EuromonitorException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public EuromonitorException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}