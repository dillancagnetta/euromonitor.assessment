﻿namespace Euromonitor.Core.Results
{
    public class Result
    {
        public bool IsSuccess { get; set; }

        protected Result(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

         protected Result() { }

        public static Result Success => new Result(true);
    }

    public class Result<T> : Result
    {
        public T Data { get; set; }

        protected Result(bool isSuccess, T data) : base (isSuccess)
        {
            IsSuccess = isSuccess;
            Data = data;
        }

        protected Result() { }

        public static Result<T> Success(T data) => new Result<T>(true, data);
    }
}
