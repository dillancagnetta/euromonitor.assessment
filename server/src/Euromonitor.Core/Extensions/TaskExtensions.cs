﻿using System.Threading.Tasks;

namespace Euromonitor.Core.Extensions
{
    public static class TaskExtensions
    {
        public static async Task<T> ConfigureAwaitable<T>(this Task<T> task)
        {
            return await task.ConfigureAwait(false);
        }

        public static async Task ConfigureAwaitable(this Task task)
        {
            await task.ConfigureAwait(false);
        }
    }
}
