﻿using System.ComponentModel.DataAnnotations;

namespace Euromonitor.Application.Books.Models
{
    public class BookInputModel : IInputModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public MoneyInputModel PurchasePrice { get; set; }
    }
}
