﻿using System.ComponentModel.DataAnnotations;

namespace Euromonitor.Application.Books.Models
{
    public class MoneyInputModel
    {
        [Required]
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
