﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;

namespace Euromonitor.Application.Books
{
    public interface IBooksAppService : IApplicationService<Book>
    {        
        Task<IEnumerable<SubscriptionViewModel>> GetBooksWithUserSubscriptionStatus(int userId);
    }
}
