﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Repositories;

namespace Euromonitor.Application.Books
{
    public class BooksAppService : ApplicationService<Book>, IBooksAppService
    {
        private readonly ISubscriptionRepository _subscriptionRepository;

        public BooksAppService(
            IBookRepository repository,
            ISubscriptionRepository subscriptionRepository,
            IMapper mapper) : base(repository, mapper)
        {
            _subscriptionRepository = subscriptionRepository;
        }
        public async Task<IEnumerable<SubscriptionViewModel>> GetBooksWithUserSubscriptionStatus(int userId)
        {
            var subscriptions = await _subscriptionRepository.GetUserSubscriptions(userId, includeInactive: false);            
            var active = subscriptions.Where(x => x.IsActive).Select( x => x.BookId);

            var vmList = new List<SubscriptionViewModel>();            
            foreach (var book in Repository.Queryable())
            {
                if (active.Contains(book.Id))
                {
                    vmList.Add( new SubscriptionViewModel{ IsActive = true, Book = book});
                    continue;
                }

                vmList.Add( new SubscriptionViewModel{ IsActive = false, Book = book});
            }

            return vmList;
        }
    }
}
