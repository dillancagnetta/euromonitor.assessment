﻿using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euromonitor.Application.Subscriptions
{
    public interface ISubscriptionsAppService : IApplicationService<UserBookSubscription>
    {
        Task<IEnumerable<SubscriptionViewModel>> GetUserSubscriptions(int userId, bool includeInactive);
        Task UpdateUserSubscription(int userId, SubscriptionInputModel model);
    }
}
