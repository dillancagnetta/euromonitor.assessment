﻿using AutoMapper;
using Euromonitor.Application.Subscriptions.Models;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euromonitor.Application.Subscriptions
{
    public class SubscriptionsAppService : ApplicationService<UserBookSubscription>, ISubscriptionsAppService
    {
        public SubscriptionsAppService(ISubscriptionRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<IEnumerable<SubscriptionViewModel>> GetUserSubscriptions(int userId, bool includeInactive)
        {
            var subscriptions = await ((ISubscriptionRepository)Repository).GetUserSubscriptions(userId, includeInactive);
            var vm = Mapper.Map<IEnumerable<SubscriptionViewModel>>(subscriptions);

            return vm;
        }

        public async Task UpdateUserSubscription(int userId, SubscriptionInputModel model)
        {
            var entity = Mapper.Map<UserBookSubscription>(model);
            await ((ISubscriptionRepository) Repository).Update(userId, entity);
            await Repository.SaveChanges();
        }
    }
}
