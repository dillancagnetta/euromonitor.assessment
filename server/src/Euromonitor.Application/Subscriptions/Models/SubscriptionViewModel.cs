﻿using Euromonitor.Domain.Model;

namespace Euromonitor.Application.Subscriptions.Models
{
    public class SubscriptionViewModel
    {
        public bool IsActive { get; set; }
        public Book Book { get; set; }
    }
}
