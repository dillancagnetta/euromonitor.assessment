﻿using System.ComponentModel.DataAnnotations;

namespace Euromonitor.Application.Subscriptions.Models
{
    public class SubscriptionInputModel : IInputModel
    {
        [Required]
        public bool IsActive { get; set; } = true;

        [Required]
        public int UserId { get; set; }

        [Required]
        public int BookId { get;  set; }
    }
}
