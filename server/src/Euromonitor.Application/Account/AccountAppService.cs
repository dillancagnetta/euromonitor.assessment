﻿using AutoMapper;
using Euromonitor.Application.Account.Models;
using Euromonitor.Domain.Model;
using Euromonitor.Domain.Model.Shared;
using Euromonitor.Domain.Repositories;
using Euromonitor.Domain.Results;
using Euromonitor.Domain.Services;
using Euromonitor.Domain.Services.Application;
using System.Threading.Tasks;

namespace Euromonitor.Application
{
    public class AccountAppService : IAccountAppService
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public AccountAppService(
            IAuthenticationService authenticationService,
            IUserRepository userRepository,
            IMapper mapper)
        {
            _authenticationService = authenticationService;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<AuthResult> AuthenticateAsync(CredentialsInputModel model)
        {
            var credentials = _mapper.Map<Credentials>(model);
            return await _authenticationService.Authenticate(credentials);
        }

        public async Task<int> RegisterAsync(RegistrationInputModel model)
        {
            var user = _mapper.Map<User>(model);
            var userId = await _userRepository.Add(user);
            return userId;
        }
    }
}
