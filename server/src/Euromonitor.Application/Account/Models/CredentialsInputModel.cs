﻿using System.ComponentModel.DataAnnotations;

namespace Euromonitor.Application.Account.Models
{
    public class CredentialsInputModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }               
    }
}
