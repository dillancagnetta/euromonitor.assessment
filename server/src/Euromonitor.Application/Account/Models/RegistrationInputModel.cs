﻿using System.ComponentModel.DataAnnotations;

namespace Euromonitor.Application.Account.Models
{
    public class RegistrationInputModel
    {
        [Required, EmailAddress]
        public string EmailAddress { get; set; }
        [Required, MinLength(4)]
        public string Password { get; set; }
        [Required, MinLength(5)]
        public string FirstName { get; set; }
        [Required, MinLength(5)]
        public string LastName { get; set; }        
    }
}
