﻿using Euromonitor.Application.Account.Models;
using Euromonitor.Domain.Results;
using System.Threading.Tasks;

namespace Euromonitor.Domain.Services.Application
{
    public interface IAccountAppService
    {
        Task<AuthResult> AuthenticateAsync(CredentialsInputModel credentials);
        Task<int> RegisterAsync(RegistrationInputModel registration);
    }
}
