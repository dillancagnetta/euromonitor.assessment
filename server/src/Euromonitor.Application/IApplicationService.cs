﻿using Euromonitor.Core.Storage.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Euromonitor.Application
{
    public interface IApplicationService<T> where T : IEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<int> CreateAsync(IInputModel model);
    }
}
