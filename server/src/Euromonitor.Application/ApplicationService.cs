﻿using Euromonitor.Core.Storage;
using Euromonitor.Core.Storage.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace Euromonitor.Application
{
    public abstract class ApplicationService<TEntity> : IApplicationService<TEntity> where TEntity : IEntity
    {
        protected IDatabaseRepository<TEntity> Repository { get; }
        public IMapper Mapper { get; }

        protected ApplicationService(
            IDatabaseRepository<TEntity> databaseRepository,
            IMapper mapper)
        {
            Repository = databaseRepository;
            Mapper = mapper;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync() => await Repository.GetAll();

        public virtual async Task<int> CreateAsync(IInputModel model)
        {
            var entity = Mapper.Map<TEntity>(model);

            await Repository.Add(entity);
            await Repository.SaveChanges();

            return entity.Id;
        }
    }
}
