﻿
using AutoMapper;
using Euromonitor.Api.Controllers;
using Euromonitor.Api.Mappings;
using Euromonitor.Application;
using Euromonitor.Application.Books;
using Euromonitor.Application.Subscriptions;
using Euromonitor.Core.Security;
using Euromonitor.Domain.Repositories;
using Euromonitor.Domain.Services;
using Euromonitor.Domain.Services.Application;
using Euromonitor.Infrastructure.Data;
using Euromonitor.Infrastructure.Repositories;
using Euromonitor.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Euromonitor.Api.Tests.Integration
{
    public class TestStartup
    {
        public TestStartup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddDatabase();
            services.AddAutoMapper( typeof(AutoMapperProfile).Assembly );
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                // Workaround due to issue: https://github.com/aspnet/Mvc/issues/5992
                .AddApplicationPart(typeof(AccountController).Assembly); 

            services.AddEuromonitorAuthentication(Configuration);

            // configure DI for application services
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<ISubscriptionRepository, SubscriptionRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserTokenService, UserTokenService>();
            services.AddScoped<IAccountAppService, AccountAppService>();   
            services.AddScoped<IBooksAppService, BooksAppService>();   
            services.AddScoped<ISubscriptionsAppService, SubscriptionsAppService>();   
        }

        public void Configure( IApplicationBuilder app )
        {
            SeedData.Initialize(app);
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();
            app.UseMvc();            
        }
    }
}
