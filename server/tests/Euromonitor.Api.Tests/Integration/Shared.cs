﻿using System.Net.Http;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Euromonitor.Api.Tests.Integration
{
    [TestClass]
    public class Shared
    {
        public static TestServer Server;
        public static HttpClient HttpClient;

        [AssemblyInitialize]
        public static void Initialize( TestContext tc )
        {
            var webHostBuilder = WebHost.CreateDefaultBuilder().UseStartup<TestStartup>();
            Server = new TestServer( webHostBuilder );
            HttpClient = Server.CreateClient();
        }
    }
}
