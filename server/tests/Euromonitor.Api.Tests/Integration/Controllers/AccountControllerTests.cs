﻿using Euromonitor.Domain.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Euromonitor.Application.Account.Models;
using Euromonitor.Domain.Model.Dto;
using Newtonsoft.Json;

namespace Euromonitor.Api.Tests.Integration.Controllers
{
    [TestClass]
    public class AccountControllerTests
    {
        private const string BaseUrl = "/api/account";

        [TestMethod]
        public async Task controller_should_authenticate_valid_user()
        {
            var emailAddress = "dillancagnetta@yahoo.com";            
            var credentials = new CredentialsInputModel
            {
                Email = emailAddress,
                Password = "password"
            };

            var response = await Shared.HttpClient.PostAsJsonAsync(BaseUrl+"/login", credentials);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var userToken = JsonConvert.DeserializeObject<UserToken>(content);

            Assert.IsNotNull(userToken);
            Assert.IsNotNull(userToken.Token);
            Assert.IsTrue(userToken.EmailAddress.Equals(emailAddress));
        }

        [TestMethod]
        public async Task controller_should_respond_with_unauthorized_when_user_invalid()
        {
            var emailAddress = "doesnt_exist@yahoo.com";
            var credentials = new CredentialsInputModel
            {
                Email = emailAddress,
                Password = "password"
            };

            var response = await Shared.HttpClient.PostAsJsonAsync(BaseUrl + "/login", credentials);

            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);  
        }

        [TestMethod]
        public async Task controller_should_register_user()
        {
            var registration = new RegistrationInputModel
            {
                EmailAddress = "new_user@gmail.com",
                Password = "password",
                FirstName = "NewUser",
                LastName = "NewUser"
            };

            var response = await Shared.HttpClient.PostAsJsonAsync(BaseUrl + "/register", registration);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [TestMethod]
        public async Task test_controller_should_get()
        {
            var response = await Shared.HttpClient.GetAsync("/api/test");
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);            
        }
    }
}
