# Euromonitor Assessment

Angular 7 Book Subscription Manager

## Running the application in docker
RUN `start-docker.bat`

App will be accessible at http://localhost:8080




## Deploying to Kubernetes with Helm
### Create Helm Packages
`helm package client/charts/euromonitorfrontend`
`helm package server/charts/euromonitorbackend`

### Install 
`helm upgrade --install --wait --version 0.1.0 --debug euromonitorfrontend  client/charts/euromonitorfrontend-v0.1.0.tgz`
`helm upgrade --install --wait --version 0.1.0 --debug euromonitorbackend  server/charts/euromonitorbackend-v0.1.0.tgz`

### Get the Host IP Address (Kubernetes)
`kubectl get pod frontend-deployment-helm-**** -o json | grep hostIP`

Accessible at http://hostIP:NodePort


