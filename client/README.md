# Euromonitor Assessment

Angular 7 Book Subscription Manager

## Build the image
`docker build -t client .`

## Run the container
`docker run -p 8080:80 client`

## Running
App will be accessible at http://localhost:8080