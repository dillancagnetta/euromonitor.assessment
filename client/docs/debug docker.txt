# client is container name
docker exec -it client /bin/ash  

# install curl
apk --no-cache add curl 

# testing
curl http://server:5000/api/test

# login
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"email":"dillancagnetta@yahoo.com","password":"password"}' \
  http://server:5000/api/account/login

 