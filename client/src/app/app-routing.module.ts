import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '@app/home';
import { AuthGuard } from '@app/_guards';
import { LoginComponent } from '@app/login';
import { RegisterComponent } from '@app/register';

const routes: Routes = [
    /*{ path: '', component: HomeComponent, canActivate: [AuthGuard] },*/
    {
        path: '',
        loadChildren: './features/subscriptions/subscriptions.module#SubscriptionsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'books',
        loadChildren: './features/books/books.module#BooksModule',
        canActivate: [AuthGuard]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
