﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ErrorInterceptor, JwtInterceptor } from './_helpers';

import { AppComponent } from './app.component';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { CoreModule } from '@app/_core/core.module';
import { AppRoutingModule } from '@app/app-routing.module';
import { AlertComponent } from '@app/_core/alert/alert.component';

;

@NgModule({
    imports: [
        // angular
        BrowserModule,
        ReactiveFormsModule,

        // core & shared
        CoreModule,

        // features


        // app
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }