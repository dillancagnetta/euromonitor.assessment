﻿import { IEntity } from '@app/_models/entity';

export class User implements IEntity {
    id: number;
    emailAddress: string;
    firstName: string;
    lastName: string;
    token: string;
}