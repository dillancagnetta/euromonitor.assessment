import { IMoney } from '@app/_models/money';
import { IEntity } from '@app/_models/entity';

export interface IBook extends IEntity {
    name: string;
    text: string;
    purchasePrice: IMoney;
}