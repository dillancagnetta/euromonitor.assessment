import { interval, Observable, of, throwError, timer } from 'rxjs';
import { mergeMap, finalize, retryWhen, flatMap } from 'rxjs/operators';
import { Logger } from '@app/_core';


const log = new Logger('Resilience Utils');

interface IResilienceOptions {
  maxRetryAttempts?: number;
  scalingDuration?: number;
  excludedStatusCodes?: number[];
}

/**
 * General Retry Strategy with scaling duration retry
 * Will throw an error when MaxRetryAttempts is reached
 * @param IResilienceOptions
 * @exception Error
 */
export const genericRetryStrategy = ({
                                     maxRetryAttempts = 3,
                                     scalingDuration = 500,
                                     excludedStatusCodes = [] }: IResilienceOptions  = {}) => (attempts: Observable<any>) => {
  return attempts.pipe(
    mergeMap((error, i) => {
      const retryAttempt = i + 1;
      // if maximum number of retries have been met
      // or response is a status code we don't wish to retry, throw error
      if (
        retryAttempt > maxRetryAttempts ||
        excludedStatusCodes.find(e => e === error.status)) {
          log.warn('max retries reached', error);
          return throwError(error);
      }
      log.debug(`Attempt ${retryAttempt}: retrying in ${retryAttempt * scalingDuration}ms`
      );
      // retry after 1s, 2s, etc...
      return timer(retryAttempt * scalingDuration);
    }),
    finalize(() => log.debug('We are done!'))
  );
};


export function  httpRetryStrategy(maxRetry: number = 5, delayMs: number = 2000) {
  return (src: Observable<any>) => src.pipe(
    retryWhen(_ => {
      return interval(delayMs)
        .pipe(
          flatMap(count => count === maxRetry ? throwError('Giving up') : of(count))
        );
    })
  );
}