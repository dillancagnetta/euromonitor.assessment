import { Injectable, isDevMode } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AlertService } from '../alert/alert.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ExceptionService {

    constructor( private _alertService: AlertService ) {
    }

    catchBadResponse: ( error: HttpErrorResponse ) => Observable<any> = ( error: any ) => {
        console.log('devMode: ', isDevMode());

        // Show client friendly message in production
        let errorMessage = 'There was a problem fulfilling your request.';

        // Show detailed message in development
        if ( isDevMode() ) {
            errorMessage =  `<b>${error.message}</b>`;

            // add error details
            if ( error.error &&  error.error.errors ) {
                errorMessage += `<br/> Details: ${error.error.errors.join( '<br/>' )}`;
            }
        }

        // handle error
        console.error( error );
        this._alertService.error( errorMessage, true );

        // do not propagate error
        return of(null);

        // propagate error
        // return Observable.throw( errorMessage );
    }
}
