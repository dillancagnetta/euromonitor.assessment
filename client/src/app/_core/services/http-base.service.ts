import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, retryWhen, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs';
import { AlertService, Logger } from '@app/_core';
import { environment } from '@environments/environment';
import { genericRetryStrategy } from '@app/_core/utilities/http.resilience';
import { AuthenticationService } from '@app/_services';

const log = new Logger( 'HttpBaseService' );

export declare type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

const httpOptions = {
    headers: new HttpHeaders( {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    } ),
    body: {},
    observe: 'response' as 'body',
    params: {}
};

@Injectable()
export abstract class HttpBaseService {

    private endpoint = environment.apiUrl;

    constructor(
        private httpClient: HttpClient,
        private alertService: AlertService,
        private authService: AuthenticationService ) {
    }

    protected get<T>( url: string, params?: any, isProtected: boolean = false ): Observable<T> {
        return this
            .request<T>( 'GET', url, null, params, isProtected )
            .pipe( map( response => response.body ) );
    }

    protected post<T>( url: string, data: any, isProtected: boolean = false ): Observable<T> {
        return this
            .request<T>( 'POST', url, data, null, isProtected )
            .pipe( map( response => response.body ) );
    }

    protected put<T>( url: string, data: any, isProtected: boolean = false ): Observable<T> {
        return this
            .request<T>( 'PUT', url, data, null, isProtected )
            .pipe( map( response => response.body ) );
    }

    protected delete<T>( url: string, isProtected: boolean = false ): Observable<T> {
        return this
            .request<T>( 'GET', url, null, null, isProtected )
            .pipe( map( response => response.body ) );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param error
     */
    protected handleError( error: HttpErrorResponse ): Observable<any> {
        this.alertService.error( 'There was a problem fulfilling your request. We have tried 3 time already.' );
        return of();
    }

    /**
     * Handle Http requests
     * Generic reusable Http Client request method
     * @param method: HttpMethod
     * @param url
     * @param data: to be posted or put
     * @param params: query string params
     * @param isProtected: is authentication needed?
     */
    private request<T>( method: HttpMethod, url: string, data?: T, params?: any, isProtected: boolean = false )
        : Observable<HttpResponse<T>> {
        if ( isProtected ) {
            const token = this.authService.getAccessToken();
            httpOptions.headers.append( 'Authorization', token );
        }

        httpOptions.body = data;
        httpOptions.params = createHttpParams(params);
        return this.httpClient.request<HttpResponse<T>>( method, `${this.endpoint}${url}`, httpOptions )
            .pipe(
                retryWhen( genericRetryStrategy() ),
                tap( response => log.debug( 'request', method, response ) ),
                catchError( error => this.handleError( error ) )
            );
    }
}

export function createHttpParams(params: {}): HttpParams {
    let httpParams: HttpParams = new HttpParams();

    if ( params ) {
        Object.keys(params).forEach(param => {
            if (params[param]) {
                httpParams = httpParams.set(param, params[param]);
            }
        });
    }

    return httpParams;
}