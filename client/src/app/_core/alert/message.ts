/**
 * Message data-structure for all messages and alerts in the application
 * */
export interface Message {
  severity?: string;
  summary?: string;
  detail?: string;
  id?: any;
}
