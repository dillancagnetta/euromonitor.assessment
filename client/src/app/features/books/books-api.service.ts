import { Injectable } from '@angular/core';
import { HttpBaseService } from '@app/_core/services/http-base.service';
import { Observable } from 'rxjs';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';

@Injectable()
export class BooksApiService extends HttpBaseService {

    public getBooks(userId: number): Observable<IUserBookSubscription[]> {
        return super.get<IUserBookSubscription[]>( '/books/user/' + userId);
    }

}
