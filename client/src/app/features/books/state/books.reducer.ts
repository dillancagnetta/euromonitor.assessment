import { Action, createSelector } from '@ngrx/store';
import { IUserBookSubscription } from '../../subscriptions/models/user-book-subscription';
import { IAppState } from '@app/app-state';

export enum BooksActionTypes {
    LOAD = '[Books] Load',
    REMOVE = '[Books] Remove',
    SUBSCRIBE = '[Books] Subscribe',
    UNSUBSCRIBE = '[Books] Unsubscribe'
}

export class ActionBooksLoad implements Action {
    readonly type = BooksActionTypes.LOAD;

    constructor(readonly payload: { books: IUserBookSubscription[] }) {}
}

export class ActionBooksRemove implements Action {
    readonly type = BooksActionTypes.REMOVE;

    constructor(readonly payload: { bookId: number }) {}
}

export class ActionBooksSubscribe implements Action {
    readonly type = BooksActionTypes.SUBSCRIBE;

    constructor(readonly payload: { bookId: number }) {}
}

export class ActionBooksUnsubscribe implements Action {
    readonly type = BooksActionTypes.UNSUBSCRIBE;

    constructor(readonly payload: { bookId: number }) {}
}

export type BookActions =
    ActionBooksLoad |
    ActionBooksRemove |
    ActionBooksSubscribe |
    ActionBooksUnsubscribe;

export const initialState: BooksState = {
    items: []
};

export const selectorBookItems = (state: IAppState) => state.books.items;

export function booksReducer(
    state: BooksState = initialState,
    action: BookActions): BooksState {

    switch (action.type) {
        case BooksActionTypes.LOAD:
            return {
                ...state,
                items: action.payload.books
            };

        case BooksActionTypes.REMOVE:
            return {
                ...state,
                items: state.items.filter((item: IUserBookSubscription) =>
                    item.book.id !== action.payload.bookId)
            };

        case BooksActionTypes.SUBSCRIBE:
            return {
                ...state,
                items: state.items.map(
                    (item: IUserBookSubscription) =>
                        item.book.id === action.payload.bookId ? { ...item, isActive: true } : item
                )
            };

        case BooksActionTypes.UNSUBSCRIBE:
            return {
                ...state,
                items: state.items.map(
                    (item: IUserBookSubscription) =>
                        item.book.id === action.payload.bookId ? { ...item, isActive: false } : item
                )
            };

        default:
            return state;
    }
}

export interface BooksState {
    items: IUserBookSubscription[];
}