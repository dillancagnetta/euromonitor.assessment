import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

@Injectable()
export class BooksEffects {
    constructor(
        private actions$: Actions<Action>
    ) {}
}