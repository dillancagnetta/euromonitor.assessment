import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';
import { IBook } from '@app/_models/book';
import { Logger } from '@app/_core';

const log = new Logger('BooksListComponent');

@Component( {
    selector: 'app-books-list',
    templateUrl: './books-list.component.html',
    styleUrls: ['./books-list.component.css']
} )
export class BooksListComponent {

    @Input() books: IUserBookSubscription[] = [];
    @Output() outSubscribe = new EventEmitter<IBook>();
    @Output() outUnsubscribe = new EventEmitter<IBook>();

    onToggle(action: string, book: IBook) {
        log.debug(action, book);

        switch ( action ) {
            case 'subscribe': {
                this.outSubscribe.emit(book);
                break;
            }
            case 'unsubscribe': {
                this.outUnsubscribe.emit(book);
                break;
            }
        }
    }

}
