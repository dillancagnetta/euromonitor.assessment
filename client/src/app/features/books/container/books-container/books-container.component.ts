import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { BooksApiService } from '@app/features/books/books-api.service';
import { Observable, Subject } from 'rxjs';
import { AuthenticationService } from '@app/_services';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';
import { IBook } from '@app/_models/book';
import { Logger } from '@app/_core';
import { SubscriptionsApiService } from '@app/features/subscriptions/subscriptions-api.service';
import { take, takeUntil } from 'rxjs/operators';
import { IAppState } from '@app/app-state';
import {
    ActionBooksLoad,
    ActionBooksRemove,
    ActionBooksSubscribe,
    ActionBooksUnsubscribe,
    selectorBookItems
} from '@app/features/books/state/books.reducer';

const log = new Logger('BooksContainerComponent');

@Component( {
    selector: 'app-books-container',
    templateUrl: './books-container.component.html',
    styleUrls: ['./books-container.component.css']
} )
export class BooksContainerComponent implements OnInit, OnDestroy {

    public books$: Observable<IUserBookSubscription[]>;
    private userId: number;

    private unsubscribe$: Subject<void> = new Subject<void>();

    constructor(
        public store: Store<IAppState>,
        private apiService: BooksApiService,
        private subsApiService: SubscriptionsApiService,
        private authService: AuthenticationService) {
    }

    ngOnInit() {
        this.userId = this.authService.currentUserValue.id;
        this.loadData();
    }

    private loadData() {
        // load from api and put in store
        this.apiService.getBooks( this.userId )
            .pipe( takeUntil( this.unsubscribe$ ) )
            .subscribe(
                (subs: IUserBookSubscription[]) => {
                    log.debug( subs );
                    this.store.dispatch( new ActionBooksLoad( {books: subs.filter( x => !x.isActive)} ) );
                }
            );

        // select books from store
        this.books$ = this.store
            .pipe(
                select(selectorBookItems),
                takeUntil(this.unsubscribe$)
            );
    }

    onSubscribe(book: IBook) {
        log.debug(book);
        this.subsApiService.createSubscription(this.userId, book.id)
            .pipe(take(1))
            .subscribe(
                () => {
                    this.store.dispatch( new ActionBooksSubscribe( {bookId: book.id} ) );
                    this.store.dispatch( new ActionBooksRemove( {bookId: book.id} ) );
                }
            );
    }

    onUnsubscribe(book: IBook) {
        log.debug(book);
        this.subsApiService.removeSubscription(this.userId, book.id)
            .pipe(take(1))
            .subscribe(
                () => this.store.dispatch( new ActionBooksUnsubscribe( {bookId: book.id} ) )
            );
    }


    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
