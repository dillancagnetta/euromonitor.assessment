import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';


import { BooksListComponent } from './components/books-list/books-list.component';
import { BooksApiService } from '@app/features/books/books-api.service';
import { BooksRoutingModule } from '@app/features/books/books-routing.module';
import { BooksContainerComponent } from './container/books-container/books-container.component';
import { SubscriptionsApiService } from '@app/features/subscriptions/subscriptions-api.service';
import { booksReducer } from '@app/features/books/state/books.reducer';
import { BooksEffects } from '@app/features/books/state/books.effects';

@NgModule( {
    imports: [
        CommonModule,
        BooksRoutingModule,

        StoreModule.forFeature('books', booksReducer),
        EffectsModule.forFeature([BooksEffects])
    ],
    declarations: [
        BooksListComponent,
        BooksContainerComponent
    ],
    providers: [
        BooksApiService,
        SubscriptionsApiService
    ]
} )
export class BooksModule {
}
