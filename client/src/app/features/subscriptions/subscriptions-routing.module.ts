import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubscriptionsContainer } from '@app/features/subscriptions/container/subscriptions-container/subscriptions-container';

const routes: Routes = [
    {
        path: 'subs',
        component: SubscriptionsContainer,
    },
    {path: '', redirectTo: 'subs'}
];

@NgModule( {
    imports: [RouterModule.forChild( routes )],
    exports: [RouterModule]
} )
export class SubscriptionsRoutingModule {
}
