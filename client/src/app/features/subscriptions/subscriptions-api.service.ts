import { Injectable } from '@angular/core';
import { HttpBaseService } from '@app/_core/services/http-base.service';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';
import { Observable } from 'rxjs';


@Injectable()
export class SubscriptionsApiService extends HttpBaseService {

    public getUserSubscriptions( userId: number, includeInactive = true ): Observable<IUserBookSubscription[]> {
        return super.get<IUserBookSubscription[]>( '/subscriptions/user/' + userId, {includeInactive} );
    }

    public createSubscription( userId: number, bookId: number ): Observable<number> {
        const data = {userId, bookId, isActive: true};
        return super.post<number>( '/subscriptions', data, true );
    }

    public removeSubscription( userId: number, bookId: number ): Observable<number> {
        return this.sendSubscriptionRequest(userId, bookId, false);
    }

    public activateSubscription( userId: number, bookId: number ): Observable<number> {
        return this.sendSubscriptionRequest(userId, bookId, true);
    }

    private sendSubscriptionRequest(userId: number, bookId: number, isActive: boolean): Observable<any> {
        return super.put<any>( '/subscriptions/user/' + userId, {bookId, isActive}, true );
    }

}
