import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';
import { IBook } from '@app/_models/book';
import { Logger } from '@app/_core';

const log = new Logger('SubscriptionsListComponent');

@Component( {
    selector: 'app-subscriptions-list',
    templateUrl: './subscriptions-list.component.html',
    styleUrls: ['./subscriptions-list.component.css']
} )
export class SubscriptionsListComponent {

    @Input() subscriptions: IUserBookSubscription[] = [];
    @Output() outSubscribe = new EventEmitter<IBook>();
    @Output() outUnsubscribe = new EventEmitter<IBook>();

    onToggle(action: string, book: IBook) {
        log.debug(action, book);

        switch ( action ) {
            case 'subscribe': {
                this.outSubscribe.emit(book);
                break;
            }
            case 'unsubscribe': {
                this.outUnsubscribe.emit(book);
                break;
            }
        }
    }
}
