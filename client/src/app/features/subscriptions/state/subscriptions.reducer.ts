import { Action, createSelector } from '@ngrx/store';
import { IUserBookSubscription } from '../../subscriptions/models/user-book-subscription';
import { IAppState } from '@app/app-state';

export enum SubscriptionsActionTypes {
    LOAD = '[Subscriptions] Load',
    REMOVE = '[Subscriptions] Remove',
    SUBSCRIBE = '[Subscriptions] Subscribe',
    UNSUBSCRIBE = '[Subscriptions] Unsubscribe'
}

export class ActionSubscriptionsLoad implements Action {
    readonly type = SubscriptionsActionTypes.LOAD;

    constructor(readonly payload: { subs: IUserBookSubscription[] }) {}
}

export class ActionSubscriptionsRemove implements Action {
    readonly type = SubscriptionsActionTypes.REMOVE;

    constructor(readonly payload: { bookId: number }) {}
}

export class ActionSubscriptionsSubscribe implements Action {
    readonly type = SubscriptionsActionTypes.SUBSCRIBE;

    constructor(readonly payload: { bookId: number }) {}
}

export class ActionSubscriptionsUnsubscribe implements Action {
    readonly type = SubscriptionsActionTypes.UNSUBSCRIBE;

    constructor(readonly payload: { bookId: number }) {}
}

export type SubscriptionsActions =
    ActionSubscriptionsLoad |
    ActionSubscriptionsRemove |
    ActionSubscriptionsSubscribe |
    ActionSubscriptionsUnsubscribe;

export const initialState: SubscriptionsState = {
    items: []
};

export const selectorSubscriptionsItems = (state: IAppState) => state.subscriptions.items;

export function subscriptionsReducer(
    state: SubscriptionsState = initialState,
    action: SubscriptionsActions): SubscriptionsState {

    switch (action.type) {
        case SubscriptionsActionTypes.LOAD:
            return {
                ...state,
                items: action.payload.subs
            };

        case SubscriptionsActionTypes.REMOVE:
            return {
                ...state,
                items: state.items.filter((item: IUserBookSubscription) =>
                    item.book.id !== action.payload.bookId)
            };

        case SubscriptionsActionTypes.SUBSCRIBE:
            return {
                ...state,
                items: state.items.map(
                    (item: IUserBookSubscription) =>
                        item.book.id === action.payload.bookId ? { ...item, isActive: true } : item
                )
            };

        case SubscriptionsActionTypes.UNSUBSCRIBE:
            return {
                ...state,
                items: state.items.map(
                    (item: IUserBookSubscription) =>
                        item.book.id === action.payload.bookId ? { ...item, isActive: false } : item
                )
            };

        default:
            return state;
    }
}

export interface SubscriptionsState {
    items: IUserBookSubscription[];
}