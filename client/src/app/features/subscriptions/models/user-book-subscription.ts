import { IBook } from '@app/_models/book';

export interface IUserBookSubscription {
    isActive: boolean;
    book: IBook;
}

