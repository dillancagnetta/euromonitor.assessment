import { SubscriptionsListComponent } from '@app/features/subscriptions/components/subscriptions-list/subscriptions-list.component';
import { SubscriptionsContainer } from '@app/features/subscriptions/container/subscriptions-container/subscriptions-container';
import { SubscriptionsRoutingModule } from '@app/features/subscriptions/subscriptions-routing.module';
import { NgModule } from '@angular/core';
import { SubscriptionsApiService } from '@app/features/subscriptions/subscriptions-api.service';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { subscriptionsReducer } from '@app/features/subscriptions/state/subscriptions.reducer';
import { SubscriptionsEffects } from '@app/features/subscriptions/state/subscriptions.effects';

@NgModule({
    imports: [
        CommonModule,
        SubscriptionsRoutingModule,

        StoreModule.forFeature('subscriptions', subscriptionsReducer),
        EffectsModule.forFeature([SubscriptionsEffects])
    ],
    declarations: [
        SubscriptionsListComponent,
        SubscriptionsContainer
    ],
    providers: [SubscriptionsApiService]
})
export class SubscriptionsModule {
}