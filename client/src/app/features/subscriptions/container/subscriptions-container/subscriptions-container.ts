import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubscriptionsApiService } from '@app/features/subscriptions/subscriptions-api.service';
import { Observable, Subject } from 'rxjs';
import { IUserBookSubscription } from '@app/features/subscriptions/models/user-book-subscription';
import { select, Store } from '@ngrx/store';
import { IAppState } from '@app/app-state';
import { AuthenticationService } from '@app/_services';
import { take, takeUntil } from 'rxjs/operators';
import { Logger } from '@app/_core';
import {
    ActionSubscriptionsLoad,
    ActionSubscriptionsSubscribe,
    ActionSubscriptionsUnsubscribe,
    selectorSubscriptionsItems
} from '@app/features/subscriptions/state/subscriptions.reducer';
import { IBook } from '@app/_models/book';

const log = new Logger('SubscriptionsContainer');

@Component( {
    selector: 'app-subscriptions-container',
    templateUrl: './subscriptions-container.html',
    styleUrls: ['./subscriptions-container.css']
} )
export class SubscriptionsContainer implements OnInit, OnDestroy {

    public subscriptions$: Observable<IUserBookSubscription[]>;
    private userId: number;

    private unsubscribe$: Subject<void> = new Subject<void>();

    constructor(
        public store: Store<IAppState>,
        private apiService: SubscriptionsApiService,
        private authService: AuthenticationService) {
    }

    ngOnInit() {
        this.userId = this.authService.currentUserValue.id;
        this.loadData();
    }

    private loadData() {
        // load from api and put in store
        this.apiService.getUserSubscriptions( this.userId )
            .pipe( takeUntil( this.unsubscribe$ ) )
            .subscribe(
                (subs: IUserBookSubscription[]) => {
                    log.debug( subs );
                    this.store.dispatch( new ActionSubscriptionsLoad( {subs} ) );
                }
            );

        // select subscriptions from store
        this.subscriptions$ = this.store
            .pipe(
                select(selectorSubscriptionsItems),
                takeUntil(this.unsubscribe$)
            );
    }

    onSubscribe(book: IBook) {
        log.debug(book);
        this.apiService.activateSubscription(this.userId, book.id)
            .pipe(take(1))
            .subscribe(
                () => this.store.dispatch( new ActionSubscriptionsSubscribe( {bookId: book.id} ) )
            );
    }

    onUnsubscribe(book: IBook) {
        log.debug(book);
        this.apiService.removeSubscription(this.userId, book.id)
            .pipe(take(1))
            .subscribe(
                () => this.store.dispatch( new ActionSubscriptionsUnsubscribe( {bookId: book.id} ) )
            );
    }


    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

}
