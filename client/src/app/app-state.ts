import { BooksState } from '@app/features/books/state/books.reducer';
import { SubscriptionsState } from '@app/features/subscriptions/state/subscriptions.reducer';


export interface IAppState {
    books: BooksState;
    subscriptions: SubscriptionsState;
}